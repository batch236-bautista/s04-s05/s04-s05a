package com.zuitt.activity;

public class Main {
    public static void main(String[] args) {
        //instantiate a new object named phonebook from the Phonebook class(default constructor)
        Phonebook phonebook = new Phonebook();

        //instantiate 2 contacts from the contact class
        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        contact1.setContactNumber("09123456789");
        contact1.setAddress("Raleigh, USA");

        Contact contact2 = new Contact();
        contact2.setName("Jane Doe");
        contact2.setContactNumber("98765432190");
        contact2.setAddress("Plymouth, USA");

        // add both contacts to the phonebook object
        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        System.out.println(contact1.getName() + " has the following registered number: " + contact1.getContactNumber());
        System.out.println(contact1.getName() + " has the following registered address: " + contact1.getAddress());
        System.out.println(contact2.getName() + " has the following registered number: " + contact2.getContactNumber());
        System.out.println(contact2.getName() + " has the following registered address: " + contact2.getAddress());

        //Define a control structure in the main method


    }
}
