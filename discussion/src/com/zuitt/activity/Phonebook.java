package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {

    //instance/variable property
    private ArrayList<String> contacts;

    //default constructor
    public Phonebook() {}

    // parameterized constructor
    /*public Phonebook(String contacts) {
        this.contacts = ArrayList<String> contacts;
    }*/

    //getter
    public ArrayList<String> getContacts() {
        return this.contacts;
    }

    //setter

    public void setContacts(ArrayList<String> contacts) {
        this.contacts = contacts;
    }
}
