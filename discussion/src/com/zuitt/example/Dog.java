package com.zuitt.example;

// Child Class of Animal Class
// "extend" keyword used to inherit the properties and methods of the parent class.
public class Dog extends Animal {

    //properties
    private String breed;

    //constructor
    public Dog(){
        //This will inherit the Animal() constructor.
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    //getter and setter
    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed){
        this.breed = breed;
    }

    // Methods
    public void speak(){
        System.out.println("Woof woof!");
    }

}

