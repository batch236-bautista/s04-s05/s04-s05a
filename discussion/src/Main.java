import com.zuitt.example.*;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");
        //OOP
        //OOP stands for "Object-Oriented Programming".
        // OOP is a programming model that allows developers to design software around data or objects, rather than function and logic.

        // OOP Concepts
        // Object - abstract idea that represents something in the real world.
        // Example: The concept of a dog
        // Class - representation of the object using code.
        // Example: Writing a code that would describe a dog.
        // Instance - unique copy of the idea, made "physical".
        // Example: Instantiating a dog names Spot from the dog class.

        // Objects
        // States and Attributes-what is the idea about?
        // Behaviors-what can idea do?
        // Example: A person has attributes like name, age, height, and weight. And a person can eat, sleep, and speak.

        //Four Pillars of OOP
        //1. Encapsulation
        // a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
        // "data hiding" -  the variables of a class will be hidden from other classes, and can be accessed only through the methods of their current class.
        //To achieve encapsulation:
        // variables/properties as private.
        // provide a public setter and getter function.
        //create a car
        Car myCar = new Car();
        myCar.drive();

        //setter
        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2015);

        //getter
        System.out.println(myCar.getName());
        System.out.println(myCar.getBrand());
        System.out.println(myCar.getYearOfMake());
        System.out.println(myCar.getDriverName());

        Car myCar2 = new Car("Kia", "Sorrento", 2022);

        //setter
        myCar2.setYearOfMake(2023);
        myCar2.setDriverName("John Smith");

        //getter
        System.out.println(myCar2.getName());
        System.out.println(myCar2.getBrand());
        System.out.println(myCar2.getYearOfMake());
        System.out.println(myCar2.getDriverName());

        // Composition and Inheritance
        // Both concepts promotes code reuse through different approach
        //"Inheritance" allows modelling an object that is a subset of another objects.
        // It defines “is a relationship”.
        // To design a class on what it is.
        //"Composition" allows modelling objects that are made up of other objects.
        // both entities are dependent on each other
        // composed object cannot exist without the other entity.
        // It defines “has a relationship”.
        // To design a class on what it does.
        // Example:
        // A car is a vehicle - inheritance
        // A car has a driver - composition

        //2. Inheritance
        // Can be defined as the process where one class acquires the properties and methods of another class.
        // With the use of inheritance the information is made manageable in hierarchical order.

        //Dog is Animal (Dog inherits Animal class)
        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        myPet.call();
        System.out.println(myPet.getName() +" "+ myPet.getColor() +" "+ myPet.getBreed());

        //3. Abstraction
            // is a process where all the logic and complexity are hidden from the user.

            // Interfaces
                // This is used to achieve total abstraction
                // Creating Abstract classes doesn't support "multiple inheritance", but it can achieve with interfaces
                // act as "contracts" wherein a class implement the interface should have the methods that the interfaces has defined in the class.

            // Person implements Actions & Greetings
        Person child = new Person();

        child.sleep();
        child.run();
        child.morningGreet();
        child.holidayGreet();

        //4. Polymorphism
            // Derived from the greek words: "poly" means many and "morph" means form.
            // Static Polymorphism
        StaticPoly myAddition = new StaticPoly();
        //original
        System.out.println(myAddition.addition(5, 4));
        //overload method based on the arguments
        System.out.println(myAddition.addition(5, 4, 6));
        //overload method based on the data types
        System.out.println(myAddition.addition(5.5, 4.4));

            //Dynamic or run-time polymorphism
                //Function is overridden by replacing the definition of the method in the parent class and the child class.
        Child myChild = new Child();
        myChild.speak();


    }
}
